import 'package:bytebank/components/editor.dart';
import 'package:bytebank/models/saldo.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const _titleAppBar = 'Receber Depósito';
const _valueFieldTip = '0,00';
const _valueFieldLabel = 'Valor';
const _confirmButtonText = 'Confirmar';

class DepositForm extends StatelessWidget {
  DepositForm({Key? key}) : super(key: key);

  final TextEditingController _valueFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_titleAppBar),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Editor(
              dica: _valueFieldTip,
              controlador: _valueFieldController,
              rotulo: _valueFieldLabel,
              icone: Icons.monetization_on,
            ),
            ElevatedButton(
              child: Text(_confirmButtonText),
              onPressed: () => _addDeposit(context),
            ),
          ],
        ),
      ),
    );
  }

  _addDeposit(context) {
    final double? value = double.tryParse(_valueFieldController.text);
    final validDeposit = _validateDeposit(value);

    if (validDeposit) {
      _updateState(context, value);

      Navigator.pop(context);
    }
  }

  _validateDeposit(value) {
    final filledField = value != null;

    return filledField;
  }

  _updateState(context, value) {
    Provider.of<Saldo>(context, listen: false).add(value);
  }
}
