import 'package:bytebank/components/editor.dart';
import 'package:bytebank/models/saldo.dart';
import 'package:bytebank/models/transferencia.dart';
import 'package:bytebank/models/transferencias.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const _tituloAppBar = 'Criando Transferência';

const _rotuloCampoValor = 'Valor';
const _dicaCampoValor = '0.00';

const _rotuloCampoNumeroConta = 'Número da conta';
const _dicaCampoNumeroConta = '0000';

const _textoBotaoConfirmar = 'Confirmar';

class TransferForm extends StatelessWidget {
  final TextEditingController _controladorCampoNumeroConta =
      TextEditingController();
  final TextEditingController _controladorCampoValor = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(_tituloAppBar),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Editor(
                controlador: _controladorCampoNumeroConta,
                dica: _dicaCampoNumeroConta,
                rotulo: _rotuloCampoNumeroConta,
              ),
              Editor(
                dica: _dicaCampoValor,
                controlador: _controladorCampoValor,
                rotulo: _rotuloCampoValor,
                icone: Icons.monetization_on,
              ),
              ElevatedButton(
                child: Text(_textoBotaoConfirmar),
                onPressed: () => _criaTransferencia(context),
              ),
            ],
          ),
        ));
  }

  void _criaTransferencia(BuildContext context) {
    final int numeroConta = int.tryParse(_controladorCampoNumeroConta.text) as int;
    final double valor = double.tryParse(_controladorCampoValor.text) as double;
    final validTransfer = _validateTransfer(context, numeroConta, valor);
    if (validTransfer) {
      final newTransfer = Transferencia(valor, numeroConta);
      _updateState(context, newTransfer, valor);
      Navigator.pop(context);
    }
  }

  _validateTransfer(context ,accountNumber, value) {
    final filledFields = accountNumber != null && value != null;
    final suficientSolde = value <= Provider.of<Saldo>(context, listen: false).value;
    return filledFields && suficientSolde;
  }

  _updateState(context, newTransfer, value ) {
    Provider.of<Transferencias>(context, listen: false).add(newTransfer);
    Provider.of<Saldo>(context, listen: false).subtract(value);
  }
}
