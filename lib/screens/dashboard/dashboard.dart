import 'package:bytebank/screens/dashboard/saldo.dart';
import 'package:bytebank/screens/deposit/deposit_form.dart';
import 'package:bytebank/screens/transferencia/transfer_form.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Bytebank"),
        ),
        body: ListView(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: SaldoCard(),
            ),
            ButtonBar(
              alignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => DepositForm()));
                      },
                      child: Text('Depósitar')),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => TransferForm()));
                      },
                      child: Text('Nova Transferência')),
                ),
              ],
            )
          ],
        ));
  }
}
