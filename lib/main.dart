import 'package:bytebank/models/saldo.dart';
import 'package:bytebank/models/transferencias.dart';
import 'package:bytebank/screens/dashboard/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Saldo(0)),
        ChangeNotifierProvider(create: (context) => Transferencias())
      ],
      child: BytebankApp(),
    ));

class BytebankApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green[900],
        colorScheme: ColorScheme(
          primary: Colors.blueAccent[700]!,
          secondary: Colors.greenAccent[700]!,
          primaryVariant: Colors.blueAccent[200]!,
          onError: Colors.red[200]!,
          onBackground: Colors.black,
          error: Colors.red,
          secondaryVariant: Colors.blue[200]!,
          onPrimary: Colors.green[200]!,
          background: Colors.lightBlueAccent,
          brightness: Brightness.light,
          surface: Colors.purple[200]!,
          onSurface: Colors.pink[200]!,
          onSecondary: Colors.brown,
        ),
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.blueAccent[700],
          textTheme: ButtonTextTheme.primary,
        ),
      ),
      home: Dashboard(),
    );
  }
}
