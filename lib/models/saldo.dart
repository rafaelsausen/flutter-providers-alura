import 'package:flutter/widgets.dart';

class Saldo extends ChangeNotifier {
  double value;

  Saldo(this.value);

  void add(double addedValue) {
    this.value += addedValue;
    notifyListeners();
  }

  void subtract(double subtractedValue) {
    if (subtractedValue > this.value) {

    }
    this.value -= subtractedValue;
    notifyListeners();
  }

  @override
  String toString() {
    return "R\$ ${value.roundToDouble().toString().replaceAll(".", ",")}";
  }
}
